package com.wsjo.json.parser.exception;

public class ParsingException extends RuntimeException {
	
	private static final long serialVersionUID = -8699045894150865549L;

	public ParsingException(String message) {
		super(message);
	}
}
