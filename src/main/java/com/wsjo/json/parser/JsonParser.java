package com.wsjo.json.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class JsonParser {
	
	// convert json string to Map object
	public static Object parse(String json) {
		JParser parser = JsonParserFactory.getParser(json);
		
		Stack<String> stack = new Stack<String>();
		Stack<Map<String, Object>> objectStack = new Stack<Map<String, Object>>();
		Stack<List<Object>> listStack = new Stack<List<Object>>();
		
		Map<String, Object> currentMap = null;
		List<Object> array = null;
		boolean isArray = false;
		
		while(parser.hasNext()) {
			ElementType element = parser.next();
			String value = parser.getValue();
			switch (element) {
				case OPEN_OBJECT:
					stack.push("OBJECT");
					if(currentMap != null)
						objectStack.push(currentMap);
					currentMap = new HashMap<String, Object>();
					isArray = false;
					break;
				case CLOSE_OBJECT:
					stack.pop();
					isArray = !stack.isEmpty() && stack.peek().equals("ARRAY");
					if(!objectStack.isEmpty()) {
						Map<String, Object> tmp = objectStack.pop();
						if(isArray)
							array.add(currentMap);
						else
							tmp.put(stack.pop(), currentMap);
						currentMap = tmp;
					}
					break;
				case OPEN_ARRAY:
					stack.push("ARRAY");
					if(array != null)
						listStack.add(array);
					array = new ArrayList<Object>();
					isArray = true;
					break;
				case CLOSE_ARRAY:
					stack.pop();
					isArray = stack.peek().equals("ARRAY");
					if(!listStack.isEmpty()) {
						List<Object> tmp = listStack.pop();
						if(isArray)
							tmp.add(array);
						else
							currentMap.put(stack.pop(), array);
						array = tmp;
					} else {
						currentMap.put(stack.pop(), array);
					}
					break;
				case KEY:
					stack.push(parser.getValue());
					break;
				case VALUE_STRING:
					if(isArray)
						array.add(value);
					else
						currentMap.put(stack.pop(), value);
					break;
				case VALUE_NUMERIC:
					if(tryIntParse(value) != null) {
						if(isArray)
							array.add(tryIntParse(value));
						else
							currentMap.put(stack.pop(), tryIntParse(value));
					} else if(tryDoubleParse(value) != null) {
						if(isArray)
							array.add(tryDoubleParse(value));
						else
							currentMap.put(stack.pop(), tryDoubleParse(value));
					}
					break;
				case VALUE_BOOLEAN:
					if(isArray)
						array.add(Boolean.parseBoolean(value));
					else
						currentMap.put(stack.pop(), Boolean.parseBoolean(value));
					break;
				case NULL:
					if(isArray)
						array.add(null);
					else
						currentMap.put(stack.pop(), null);
					break;
				default:
					break;
			}
		}
		
		return currentMap;
	}
	
	private static Integer tryIntParse(String s) {
		try {
			return Integer.parseInt(s);
		} catch(NumberFormatException ne) {
			return null;
		}
	}
	
	private static Double tryDoubleParse(String s) {
		try {
			return Double.parseDouble(s);
		} catch(NumberFormatException ne) {
			return null;
		}
	}

}
