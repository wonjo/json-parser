package com.wsjo.json.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Stack;

import com.wsjo.json.parser.exception.ParsingException;

// implementation of simple json parser
public class JParserImpl implements JParser {

	private Stack<State> stack = new Stack<State>();
	
	private ElementType currentElementType;
	
	private int nextChar = -1;
	
	private String value;
	
	private Reader jsonReader;
	
	public JParserImpl(InputStream json) {
		this.jsonReader = new InputStreamReader(json);
	}

	public boolean hasNext() {
		if(stack.isEmpty() && (currentElementType == ElementType.CLOSE_ARRAY || currentElementType == ElementType.CLOSE_OBJECT)) {
			return false;
		}
		return true;
	}

	public ElementType next() {
		try {
			int c = nextChar == -1 ? jsonReader.read() : nextChar;
			nextChar = -1;
			StringBuffer b = new StringBuffer();
			
			int commaCounter = 0;
			int colonCounter = 0;
			
			while(c == ',' || c == ':' || c == ' ' | c == '\n' | c == '\t') {
			    if(c == ',') {
			        if(commaCounter == 0)
			            commaCounter++;
			        else
			            throw new ParsingException("too many commas");
			    } else if(c == ':') {
			        if(colonCounter == 0)
			            colonCounter++;
			        else
			            throw new ParsingException("too many colons");
			    }
			    c = jsonReader.read();
			}
			
			switch (c) {
			case '{':
				stack.push(State.OBJECT);
				currentElementType = ElementType.OPEN_OBJECT;
				return ElementType.OPEN_OBJECT;
			case '}':
				if(stack.pop() == State.OBJECT) {
					currentElementType = ElementType.CLOSE_OBJECT;
					return ElementType.CLOSE_OBJECT;
				} else {
					throw new ParsingException("No correspoding open curly bracket");
				}
			case '[':
				currentElementType = ElementType.OPEN_ARRAY;
				stack.push(State.ARRAY);
				return ElementType.OPEN_ARRAY;
			case ']':
			    if(stack.pop() == State.ARRAY) {
			        currentElementType = ElementType.CLOSE_ARRAY;
			        return ElementType.CLOSE_ARRAY;
			    } else {
			        throw new ParsingException("No correspoing open square bracket");
			    }
			case '"':
			    c = jsonReader.read();
                while(c != '"') {
                    b.append((char) c);
                    c = jsonReader.read();
                }
                value = b.toString();  // maybe avoid string creation for optimization
			    
                if(stack.peek() == State.ARRAY || currentElementType == ElementType.KEY) {
			        currentElementType = ElementType.VALUE_STRING;
			        return ElementType.VALUE_STRING;
			    } else {
			        currentElementType = ElementType.KEY;
			        return ElementType.KEY;
			    }
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			    if(stack.peek() == State.ARRAY || currentElementType == ElementType.KEY) {
			    	int counter = 0;
			        while((c >= '0' && c <= '9') || (c == '.' && counter == 0)) {
			        	if(c == '.') 
			        		counter++;
			            b.append((char)c);
			            c = jsonReader.read();
			        }
			        nextChar = c;
			        value = b.toString();
			        currentElementType = ElementType.VALUE_NUMERIC;
			        return ElementType.VALUE_NUMERIC;
			    } else {
			        throw new ParsingException("expecting string/key, got number");
			    }
			case 'f':
			case 't':
			    if(stack.peek() == State.ARRAY || currentElementType == ElementType.KEY) {
			        while(c >= 'a' && c <= 'z') {
			            b.append((char) c);
			            c = jsonReader.read();
			        }
			        nextChar = c;
			        value = b.toString();
			        if(!value.equals("true") && !value.equals("false")) {
			            throw new ParsingException("wrong value type : " + value);
			        }
			        currentElementType = ElementType.VALUE_BOOLEAN;
			        return ElementType.VALUE_BOOLEAN;
			    } else {
			        throw new ParsingException("expecting string/key, got number");
			    }
			case 'n':
			    if(stack.peek() == State.ARRAY || currentElementType == ElementType.KEY) {
			        while(c >= 'a' && c <= 'z') {
			            b.append((char) c);
			            c = jsonReader.read();
			        }
			        nextChar = c;
			        value = b.toString();
			        if(!value.equals("null")) {
			            throw new ParsingException("wrong value type : " + value);
			        }
			        currentElementType = ElementType.NULL;
			        return ElementType.NULL;
			    }
			default:
			    
				break;
			}
		} catch (IOException e) {
			throw new ParsingException("Error while reading a char");
		}
		
		return null;
	}

    public String getValue() {
        return value;
    }

	public void close() throws IOException {
		if(jsonReader != null)
			jsonReader.close();
		
	}
}
