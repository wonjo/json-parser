package com.wsjo.json.parser;

public enum ElementType {
	
	OPEN_ARRAY,
	CLOSE_ARRAY,
	OPEN_OBJECT,
	CLOSE_OBJECT,
	KEY,
	VALUE_STRING,
	VALUE_NUMERIC,
	VALUE_BOOLEAN,
	NULL
}
