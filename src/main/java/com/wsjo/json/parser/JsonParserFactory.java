package com.wsjo.json.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class JsonParserFactory {
	
	public static JParser getParser(String json) {
		InputStream is = new ByteArrayInputStream(json.getBytes());
		return new JParserImpl(is);
	}
	
	public static JParser getParser(InputStream json) {
	    return new JParserImpl(json);
	}
}
