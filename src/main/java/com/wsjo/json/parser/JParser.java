package com.wsjo.json.parser;

import java.io.Closeable;

// interface for basic json parser
public interface JParser extends Closeable{
	
	public boolean hasNext();
	
	public ElementType next();
	
	public String getValue();
}
