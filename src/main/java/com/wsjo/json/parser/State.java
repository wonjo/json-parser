package com.wsjo.json.parser;

public enum State {
	OBJECT,
	ARRAY,
	OTHER
}
