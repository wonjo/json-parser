package com.wsjo.json.parser.util;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {
    
    public String read(String file) throws IOException {
        URL url = getClass().getResource(file);
        
        byte[] encoded = Files.readAllBytes(Paths.get(url.getPath()));
        
        return new String(encoded, "UTF-8");
    }
}
