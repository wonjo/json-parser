package com.wsjo.json.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wsjo.json.parser.util.FileUtil;

public class JsonParserTest {
	
	FileUtil fileUtil;
	String json;
	
	@Before
	public void setup() {
		fileUtil = new FileUtil();
	}
	
	@Test
	public void basicTest() {
		try {
			json = fileUtil.read("/documents/basic-test.json");
			Map<String, Object> output = (Map<String, Object>)JsonParser.parse(json);
			Assert.assertTrue(output.get("debug").equals("on"));
			Map<String, Object> window = (Map<String, Object>)output.get("window");
			Assert.assertTrue(window.get("title").equals("sample"));
			Assert.assertTrue(window.get("size").equals(500));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void basicWithArrayTest() throws IOException {
		json = fileUtil.read("/documents/basic-test-with-array.json");
		Map<String, Object> output = (Map<String, Object>)JsonParser.parse(json);
		Assert.assertTrue(output.get("debug").equals("on"));
		Map<String, Object> window = (Map<String, Object>)output.get("window");
		Assert.assertTrue(window.get("title").equals("sample"));
		Assert.assertTrue(window.get("size").equals(500));
		List<Object> array = (List<Object>)output.get("array");
		Assert.assertTrue(array.size() == 4);
		Assert.assertTrue(array.contains(123));
		Assert.assertTrue(array.contains("567"));
		Assert.assertTrue(array.contains(true));
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("location", "london");
		Assert.assertTrue(array.contains(map));
	}
	
	@Test
	public void fullTest() throws IOException {
		json = fileUtil.read("/documents/full-test.json");
		Map<String, Object> output = (Map<String, Object>)JsonParser.parse(json);
		Assert.assertTrue(output.get("name").equals("hello"));
		Assert.assertTrue(output.get("message").equals("world"));
		Assert.assertTrue(output.get("numeric").equals(1234));
		Assert.assertTrue(output.get("decimal").equals(1.245));
		Assert.assertTrue(output.get("boolean-true").equals(true));
		Assert.assertTrue(output.get("boolean-false").equals(false));
		Assert.assertTrue(output.get("unicode").equals("바탕"));
		Assert.assertTrue(output.get("null") == null);
		
		List<Object> array = (List<Object>)output.get("array");
		Assert.assertTrue(array.size() == 7);
		Assert.assertTrue(array.contains("element1"));
		Assert.assertTrue(array.contains("element2"));
		Assert.assertTrue(array.contains(1234));
		Assert.assertTrue(array.contains(true));
		Assert.assertTrue(array.contains(null));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("xyz", "xyz");
		List<Object> subArray = new ArrayList<Object>();
		subArray.add("a");
		subArray.add("b");
		subArray.add("c");
		map.put("sub-array", subArray);
		
		Assert.assertTrue(array.contains(map));
		
		List<Object> subArray2 = new ArrayList<Object>();
		subArray2.add(1);
		subArray2.add(2);
		Map<String, Object> subMap = new HashMap<String, Object>();
		subMap.put("abc", "abc");
		subArray2.add(subMap);
		
		Assert.assertTrue(array.contains(subArray2));
	}

}
